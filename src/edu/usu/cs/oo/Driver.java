package edu.usu.cs.oo;

public class Driver 
{
	
	public static void main(String[] args)
	{
		Student student = new Student("Riley Oakden", "A01127840", new Job("Taxidermist", 34200, new Company("StuffinStuff", new Location("100 Main Street","84341", "Logan", "Utah"))));
		
		/*
		 * Instantiate an instance of Student that includes your own personal information
		 * Feel free to make up information if you would like.
		 */
		
		
		System.out.println(student);
		
		/*
		 * Print out the student information. 
		 */
	}

}